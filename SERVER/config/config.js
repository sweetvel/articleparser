const config = {

		/* ALLOWED ORIGINS */
	allowedOrigins : [

		'http://localhost:8080',
		'http://localhost:8088',
		'http://test.sweetvel.com:8081',
		'http://test.sweetvel.com'

	]

}

module.exports = config;