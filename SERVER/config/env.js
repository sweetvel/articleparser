var API_PREFIX  = '';


const ENV = {

	dev : {

		DB_CONNECTION : "mongodb://localhost:27017/dagbladetapi",
		API_PREFIX : API_PREFIX,
		API_PORT : 4424,
},

	prod : {

		DB_CONNECTION : "mongodb://login:pass@localhost:27017/dagbladetapi",
		API_PREFIX : API_PREFIX,
		API_PORT : 4424,
	}

}

module.exports = ENV;