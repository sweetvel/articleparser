const request = require('request');
const cheerio = require('cheerio');
const url = require('url');
const suggestionsmodel = require('../models/suggestions');


class articleController {


	constructor() {

	}


	parseArticle(req, res) {

		var paragraphsArr = [], title;

		request(req.query.articleURL, function (error, response, body) {

			if (error)
				return res.status(400).send({ error: error, status: 400, statusText: "Something Went Wrong Check if Url is Correct" });

			//PARSING PAGE
			const $ = cheerio.load(body);
			title = $('h2.headline').text();
			$("p").each(function () { paragraphsArr.push($(this).text()) });

			res.status(200).send({ statusText: "ok", title: title, paragraphs: paragraphsArr });

		})

	};


	changeArticle(req, res, err) {

		var data = {

			articleUrl: req.body.articleUrl,
			originalText: req.body.originalText,
			usersText: req.body.usersText

		}

		new suggestionsmodel(data).save((err, event) => {

			if (err)
				res.status(400).send({ status: 400, statusText: err });

			else
				res.status(200).send({ statusText: "ok", data: data });

		});

	}


	suggestions(req, res, err) {

		var match =  { $match : { isApproved : JSON.parse(req.query.approved) || false } };

		var group = { $group: { _id: "$originalText" , suggestions: { $push: { id: "$_id", articleurl : '$articleUrl' , text: "$usersText" } } } };

		suggestionsmodel.aggregate([match , group]).exec((err, data) => {

			res.status(200).send({ statusText: "ok", data: data });

		})

	}


	approveSuggestion(req, res, err) {

		suggestionsmodel.findByIdAndUpdate( req.params.id , { $set: { isApproved : true }  }, (err) => {

			if (err)
				return res.status(400).send({ 'statusText': err.code });

			return res.status(200).send({ status: 200, statusText: 'successfully approved' });

		})

	}


	removeSuggestions(req, res, err){

		suggestionsmodel.remove({ _id :{ $in : req.body.idarray }} , (err , data) =>{

			if (err)
				return res.status(400).send({ 'statusText': err.code });

			return res.status(200).send({ status: 200, statusText: 'successfully removed' });


		})
	}


	addSuggestion(req, res, err) {

		var data = {

			articleUrl: req.body.articleUrl,
			originalText: req.body.originalText,
			usersText: req.body.usersText

		}

		new suggestionsmodel(data).save((err, event) => {

			if (err)
				res.status(400).send({ status: 400, statusText: err });

			else
				res.status(200).send({ status : 200 , statusText: "ok", data: data });

		});

	}

}


module.exports = new articleController();