var mongoose = require("mongoose");

const suggestionsSchema = new mongoose.Schema({


	articleUrl: {
		type: String,
		required: true,
		default : ""
	},

	originalText :{
		type: String,
		required: true,
		default : ""
	},

	usersText: {
		type: String,
		required: true,
		default: ""
	},

	isApproved :{
		type: Boolean,
		required : true,
		default : false
	},

	suggestions : {
		type : Array,
		default : []
	}

});

var suggestions = mongoose.model('suggestions',  suggestionsSchema );

module.exports = suggestions;
