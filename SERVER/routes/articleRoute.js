const express = require('express');
const router = express.Router();
const articleCtl = require('../controllers/articleController');
var url  = require('url');


/* PARSE URL

#GET - PARAMS

url     : String

*/

router.get('/article/parse',  articleCtl.parseArticle);

/* CHANGE ARTICLE

#POST - BODY

articleUrl   : String
originalText : String
usersText   : String

*/

router.post('/article/change',  articleCtl.changeArticle);

/* GET SUGGESTIONS

#GET

*/

router.get('/article/suggestions',  articleCtl.suggestions);

/* APPROVE SUGGESTION

#PUT - PARAMS

id    : String

*/

router.put('/article/approve/:id',  articleCtl.approveSuggestion);


/* DELETE ALL SUGGESTIONS

#DELETE - BODY

idarray    :  Array

*/

router.delete('/article/remove',  articleCtl.removeSuggestions);


/* ADD SUGGESTION

#POST - BODY

articleUrl   : String
originalText : String
usersText   : String

*/

router.post('/article/add',  articleCtl.addSuggestion);


module.exports = router;