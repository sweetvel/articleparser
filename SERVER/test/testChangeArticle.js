var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();

chai.use(chaiHttp);


describe('ARTICLE CHANGE', function () {

	it('CHECK ARTICLE CHANGE', function (done) {

		chai.request(server)
			.post('/article/change')
			.send({ 'articleUrl': 'https://sweetvel.com', 'originalText': 'SomeText' , 'usersText': 'SomeUsersText'  })
			.end(function (err, res) {
				res.should.have.status(200);
				res.should.be.json;
				res.body.should.be.a('object');
				res.body.should.have.property('statusText');
				res.body.should.have.property('data');
				res.body.statusText.should.equal('ok');
				done();
			});

	});

})