var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var expect =chai.expect();

chai.use(chaiHttp);


describe('ARTICLE PARSE GET', function () {

	it('should return 200', function (done) {
		chai.request(server)
			.get('/article/parse?articleURL=https://sweetvel.com')
			.end(function (err, res) {
				res.should.have.status(200);
				done();
			});
	});


	it('should return Object', function (done) {
		chai.request(server)
			.get('/article/parse?articleURL=https://sweetvel.com')
			.end(function (err, res) {
				res.should.be.a('object');
				done();
			});
	});


	it('should return JSON', function (done) {
		chai.request(server)
			.get('/article/parse?articleURL=https://sweetvel.com')
			.end(function (err, res) {
			 	res.should.be.json;
				done();
			});
	});


	it('should have Properties Title and Paragraphs', function (done) {
		chai.request(server)
			.get('/article/parse?articleURL=https://sweetvel.com')
			.end(function (err, res) {
				res.body.should.have.property('title');
				res.body.should.have.property('paragraphs');
				done();
			});
	});

});