import * as types from '../constants/ActionTypes';

export function addFriend(name) {

		return {
			type: types.ADD_USER,
			user: { UserName: name }
		}
	}

	export function deleteFriend(name) {

		return  {
			type: types.DELETE_USER,
			user: { UserName: name }
		}
	}