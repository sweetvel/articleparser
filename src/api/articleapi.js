import axios from 'axios';


export class ArticleAPI {


	static parseArticle(url) {

		return axios.get(`${_ENV_.API.PARSE_URL}?articleURL=${url}`)

	}


	static changeArticle(data) {

		return axios({

			method: 'post',
			url:_ENV_.API.CHANGE_ARTICLE,
			data: data,

		})

	}


	static getSuggestions(approved) {

		return axios.get(`${_ENV_.API.SUGGESTIONS}?approved=${approved}`)

	}


	static approveSuggestion(id) {

		return axios({

			method: 'put',
			url:`${_ENV_.API.APPROVE}/${id}`,

		})

	}


	static addSuggestion(data) {

		return axios({

			method: 'post',
			url:_ENV_.API.ADD,
			data: data,

		})

	}


	static removeSuggestions(idarray) {

		return axios({

			method: 'delete',
			url:`${_ENV_.API.REMOVE}`,
			data : { idarray : idarray }

		})

	}


}

