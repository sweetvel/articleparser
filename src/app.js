import * as React from "react";
import * as ReactDOM from "react-dom";

import userDash from './containers/userdash.jsx';
import adminDash from './containers/admindash.jsx';
import { Provider } from 'react-redux';
import { createStore } from 'redux'
import reducer from './reducers/index';
import store from './reducers/index.js';
import routes from './routes.js';

import "./style.scss";


//console.log(store.getState());


ReactDOM.render(

	<Provider store={store}>
		{routes}
	</Provider>,

	document.querySelector("#root")

);



