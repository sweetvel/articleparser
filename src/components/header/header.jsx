import * as React from "react";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import RcIf, { RcElse } from 'rc-if';
import './header.scss';

class Header extends React.Component {

	constructor(props) {

		super(props)
		this.adminRoute = location.href.split('/').splice(-1)[0].match('admin') ? true : false;

	}

	render() {

		return (
			<div id="header">
				<i className="fa fa-user-circle"></i>
				{this.props.title}
				<div className="horflexer"></div>

				<RcIf if={this.adminRoute} >
					<Link to='/'>
						<i className="fa fa-sign-in"></i>
						VISITOR
					</Link>
				</RcIf>

				<RcIf if={!this.adminRoute} >
					<Link to='/admin?showApproved=false'>
						<i className="fa fa-sign-in"></i>
						ADMIN
				</Link>
				</RcIf>

			</div>
		)
	}

};


function FetchReducersData(state) {
	return {
		ReducerStoredData: state.addata,
	}
}


export default connect(
	state => (FetchReducersData),
	dispatch => ({

	})
)(Header);