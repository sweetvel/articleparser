import * as React from "react";
import './parsearticel.scss';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { ArticleAPI } from '../../api/articleapi';
import Button from '../../sharedcomponents/button/button.jsx';
import RcIf, { RcElse } from 'rc-if';


export class ParseArticle extends React.Component {


	constructor(props) {

		super(props);

		/* HERE WE DECLARE OUR GLOBAL VARIABLES */
		this.state = {

			articleUrl: '',
			originalText: '',
			usersText: '',
			title: '',
			paragraphs: [],
			error: ''

		};

	}


	componentDidMount() {

	}


	parseArticle() {

		ArticleAPI.parseArticle(this.state.articleUrl).then(

			res =>  this.setState({ paragraphs: res.data.paragraphs, title: res.data.title , error : '' }) ,

			err =>  this.setState({ error: err.response.data.statusText })

		)

	};


	changeArticle(originalText) {

		this.state.originalText = originalText;

		ArticleAPI.changeArticle(this.state).then(

			res => { console.log(res.data) ; alert("sended") },

			err => console.log(err)

		)

	};


	render() {

		return (

			<div className="row " id="parsearticle">

				<div className="col-12 mx-auto ">
					<form className="w-100 parseUrlBlock">
						<div className="form-group">
							<label >Parse Url</label>
							<input onChange={(e) => this.setState({ articleUrl: e.target.value })} placeholder="Link" type="text" className="form-control" />
						</div>
						<Button onClick={() => this.parseArticle()} btnval="parse" />
					</form>
				</div>

				<div className="col-12 mx-auto outputBlock">

					<RcIf if={this.state.error.length > 0} >
						<div className="alert alert-danger">
							<strong>ERROR :</strong> {this.state.error}
						</div>
					</RcIf>

					<RcIf if={this.state.title && this.state.title.length > 0} >
						<div className="alert alert-success">
							<strong>TITLE :</strong> {this.state.title}
						</div>
					</RcIf>
				</div>

				<div className="col-12 mx-auto fetchedBlock">

					{this.state.paragraphs && this.state.paragraphs.map((res, ind) =>
						<ul className = "" key={ind}>
							<li>
								<b>ORIGINAL TEXT</b>
								<p>{res}</p>
								<b>USERS TEXT</b>
								<textarea onChange={(e) => this.setState({ usersText: e.target.value })} placeholder="Suggest Changes" type="text" className="form-control"></textarea>
								<Button onClick={() => this.changeArticle(res)} btnval="send changes" />
							</li>
						</ul>
					)}

				</div>

			</div>
		)

	}


};