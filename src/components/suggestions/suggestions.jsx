import React ,{PropTypes} from "react";
import { Link } from 'react-router-dom';
import './suggestions.scss';
import { ArticleAPI } from '../../api/articleapi';
import Button from '../../sharedcomponents/button/button.jsx';
import RcIf, { RcElse } from 'rc-if';
import queryString from 'query-string';
import createHistory from "history/createBrowserHistory";
const history = createHistory()


export class Suggestions extends React.Component {

	constructor(props) {

		super(props);

		/* HERE WE DECLARE OUR GLOBAL VARIABLES */
		this.state = {

			originalText: '',
			newSuggestion : '',
			suggestionsData: [],
			showApproved: queryString.parse(location.search).showApproved || false

		};

	}


	componentDidMount() {

		//INIT DATA
		this.getSuggestions();

		//LISTEN URL CHANGES
		const unlisten = history.listen((location, action) => { /* ACTIONS */} )

	}


	getSuggestions() {

		ArticleAPI.getSuggestions(this.state.showApproved).then(

			res => { this.setState({ suggestionsData: res.data.data }); console.log(res.data); },

			err => console.error(err.response.data)
		)

	}


	approveSuggestion(id) {

		ArticleAPI.approveSuggestion(id).then(

			res => { res.data.status === 200 ? this.getSuggestions() : null },

			err => console.error(err.response.data)
		)

	}


	deleteSuggestions(suggestions) {

		var idArray = [];
		suggestions.forEach(el => idArray.push(el.id))

		ArticleAPI.removeSuggestions(idArray).then(

			res => { res.data.status === 200 ? this.getSuggestions() : null; console.log(res.data) },

			err => console.error(err.response.data)
		)

	}


	addSuggestion(input) {

		var data = {};
		data.usersText = this.state.newSuggestion;
		data.originalText = input._id,
		data.articleUrl = input.suggestions[0].articleurl;

		ArticleAPI.addSuggestion(data).then(

			res => { res.data.status === 200 ? this.getSuggestions() : null; console.log(res.data) },

			err => console.error(err.response.data)
		)

		console.log("data",data)

	}


	changeStatus(status) {

		this.state.showApproved = status;
		history.push(`/admin?showApproved=${status}`)
		this.getSuggestions();

	}


	render() {

		return (

			<div className="row " id="suggestions">

				<div className="col-12 mx-auto ">

					<div className="statusBtnGroup">
						<Button onClick={() => this.changeStatus(false)} btnval="Pending" />
						<Button onClick={() => this.changeStatus(true)} btnval="Approved" />
					</div>

					{this.state.suggestionsData && this.state.suggestionsData.map((res, ind) =>

						<div key={ind} className="suggestionsBlock" >

							<b >ORIGINAL TEXT</b>

							<div className="suggestionsOriginalText">
								<p>{res._id}</p>
								<Button onClick={() => this.deleteSuggestions(res.suggestions)} kind = "danger" btnval="Delete" />
							</div>

							<b>USERS TEXT</b>

							{res.suggestions && res.suggestions.map((res, ind) =>

								<div key={ind} className="suggestionlist">
									<p>{res.text}</p>
									<Button onClick={() => this.approveSuggestion(res.id)}  kind = "success"  btnval="Approve" />
								</div>

							)}

							<div className = "additionalSuggestionBlock">
								<input placeholder = "Your suggestion"  onChange={(e) => this.setState({newSuggestion: e.target.value })} type = "text"  />
								<Button onClick={() => this.addSuggestion(res)} kind = "success" btnval="Add" />
							</div>

						</div>

					)}

				</div>

			</div>

		)

	}


}

