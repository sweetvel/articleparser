var React = require("react");
import Header from '../components/header/header.jsx';
import { Link } from 'react-router-dom';
import { Suggestions } from '../components/suggestions/suggestions.jsx';


export default class adminDash extends React.Component {

	constructor(props) {
		super(props);

	}

	render() {

		return (
			<div className="container">
				<Header title="ADMIN" title2 = "VISITOR" />
				<Suggestions />
			</div>
		)
	}
};