var React = require("react");
import Header from '../components/header/header.jsx';
import { ParseArticle } from '../components/parsearticle/parsearticle.jsx';


export default class userDash extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		return (

			<div className = "container">
				<Header title = "VISITOR" title2 = "ADMIN" />
				<ParseArticle />
			</div>

		)
	}

};



