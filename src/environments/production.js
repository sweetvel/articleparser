var HOST = "http://test.sweetvel.com:8081";
var API_URL = "http://test.sweetvel.com:4424";
var API_PREFIX = "";


const environment = {

	production: false,
	HOST: HOST,
	API_URL : API_URL,

	API: {

		PARSE_URL: `${API_URL}/${API_PREFIX}article/parse`,
		CHANGE_ARTICLE: `${API_URL}/${API_PREFIX}article/change`,
		SUGGESTIONS: `${API_URL}/${API_PREFIX}article/suggestions`,
		APPROVE: `${API_URL}/${API_PREFIX}article/approve`,
		REMOVE: `${API_URL}/${API_PREFIX}article/remove`,
		ADD: `${API_URL}/${API_PREFIX}article/add`

	}

}


module.exports = environment;