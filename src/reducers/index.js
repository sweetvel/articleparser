import { combineReducers, createStore } from 'redux'
import addata from './addata'


var reducer = combineReducers({
	addata
})

var store = createStore(reducer)

export default store