import React from 'react';
import { Router, Route, BrowserRouter, Switch } from 'react-router-dom';
import adminDash from './containers/admindash.jsx';
import userDash from './containers/userdash.jsx';


const routes = (

	<BrowserRouter >
		<Switch>
			<Route exact path="/" component={userDash} />
			<Route exact path="/admin" component={adminDash} />
		</Switch>
	</BrowserRouter>

);


export default routes;