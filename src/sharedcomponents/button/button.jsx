import * as React from "react";
import './button.scss';

export default class Button extends React.Component {

	constructor(props) {

		super(props);
		this.state = { };

	}


	setDefaultStyle(kind){

	}


	render() {

		return (

			<div  className = {

				this.props.kind === "success" ? "btnSuccess":
				(this.props.kind === "danger" ? "btnDanger" : null)

				}

				onClick={this.props.onClick} id="customBtn">
				<i className="fa fa-check"></i>
				<span className="btnText">{this.props.btnval}</span>
			</div>

		)

	}

};



